db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);



db.users.find({$or:[{firstName:{$regex:"n", $options: "$i"}},{lastName:{$regex:"d", $options: "$i"}}]});




db.users.find(
    {firstName:"Jane" },
    {
        firstName:1,
        lastName:1,
        _id:0
    }
    );




db.users.find({$or:[
    {department:"HR"},
    {age:{$gte:70}}
    ]
}
)
;




db.users.find({$and:[{age:{$ne:82}},{age:{$ne:76 }}]});

db.users.find({firstName:{$regex:"N", $options: "$i"}});

